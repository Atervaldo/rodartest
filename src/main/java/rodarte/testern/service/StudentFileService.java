
package rodarte.testern.service;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.OptionalDouble;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.WorkbookUtil;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import rodarte.testern.banco.Grade;
import rodarte.testern.banco.Student;

public class StudentFileService {

    public List<Student> readFile(InputStream arquivo) throws Exception {
        if (arquivo == null) {
            return Collections.emptyList();
        }

        List<Student> students = new ArrayList<Student>(0);

        // Chartset cellCharset=StandardCharsets.UTF_8;
        // Use an InputStream, needs more memory
        try (Workbook wb = new XSSFWorkbook(arquivo)) {

            for (Sheet sheet : wb) {
                // Pulando primeira linha = cabecalho
                for (int rownum = 1; rownum <= sheet.getLastRowNum(); rownum++) {
                    Row row = sheet.getRow(rownum);

                    // Cria um estudante por linha
                    Student student = new Student();
                    students.add(student);

                    for (Cell cell : row) {
                        switch (cell.getColumnIndex()) {
                            case 0:
                                student.setCode((int) cell.getNumericCellValue());
                                break;
                            case 1:
                                student.setName(cell.getRichStringCellValue().getString());
                                break;
                            case 2:
                                student.setSex(cell.getRichStringCellValue().getString());
                                break;
                            case 3:
                                student.setBirthDate(cell.getDateCellValue());
                                break;
                            case 4:
                            case 5:
                            case 6:
                                student.adicionarNota(2019, cell.getColumnIndex() - 3, cell.getNumericCellValue());
                                break;

                        }
                    }
                }
            }
        }

        students.forEach(student -> {
            System.out.println("Estudante => " + student);
            System.out.println("Estudante.notas => " + student.getGrades());
        });

        return students;
    }

    public void writeFile(OutputStream fileOut,
                          List<Student> arg1)
            throws Exception {

        Workbook wb = new XSSFWorkbook();
        escreverAba1(wb, arg1);
        escreverAba2(wb, arg1);
        escreverAba3(wb, arg1);

        wb.write(fileOut);
        wb.close();

    }

    private void escreverAba1(Workbook wb,
                              List<Student> students) {
        CreationHelper ch = wb.getCreationHelper();
        Sheet sheet = wb.createSheet(WorkbookUtil.createSafeSheetName("Planilha1"));

        Font cellFont = wb.createFont();
        cellFont.setBold(true);

        CellStyle cellBoldStyle = wb.createCellStyle();
        cellBoldStyle.setFont(cellFont);

        // Create header
        // Create a row and put some cells in it. Rows are 0 based.
        Row row = null;
        row = sheet.createRow(0);

        // Create a cell and put a value on it.
        Cell cell = null;

        cell = row.createCell(0);
        cell.setCellValue(ch.createRichTextString("Identificação"));
        cell.setCellStyle(cellBoldStyle);

        cell = row.createCell(1);
        cell.setCellValue(ch.createRichTextString("Nome"));
        cell.setCellStyle(cellBoldStyle);

        cell = row.createCell(2);
        cell.setCellValue(ch.createRichTextString("Sexo"));
        cell.setCellStyle(cellBoldStyle);

        cell = row.createCell(3);
        cell.setCellValue(ch.createRichTextString("Data Nascimento"));
        cell.setCellStyle(cellBoldStyle);

        cell = row.createCell(4);
        cell.setCellValue(ch.createRichTextString("Nota 1º Trimestre"));
        cell.setCellStyle(cellBoldStyle);

        cell = row.createCell(5);
        cell.setCellValue(ch.createRichTextString("Nota 2º Trimestre"));
        cell.setCellStyle(cellBoldStyle);

        cell = row.createCell(6);
        cell.setCellValue(ch.createRichTextString("Nota 3º Trimestre"));
        cell.setCellStyle(cellBoldStyle);

        // Ordenando por nome
        Collections.sort(students, Student.nameComparator());

        // Criando linhas
        for (int i = 0; i < students.size(); i++) {
            Student student = students.get(i);

            row = sheet.createRow(i + 1);

            cell = row.createCell(0);
            cell.setCellValue(student.getCode());

            cell = row.createCell(1);
            cell.setCellValue(ch.createRichTextString(student.getName()));

            cell = row.createCell(2);
            cell.setCellValue(ch.createRichTextString(student.getSex()));

            cell = row.createCell(3);

            CellStyle cell3Style = wb.createCellStyle();
            cell3Style.setDataFormat(ch.createDataFormat().getFormat("d/m/yyyy"));

            cell.setCellStyle(cell3Style);
            cell.setCellValue(student.getBirthDate());

            List<Grade> grades = student.getGrades();

            // Ordenar por ano / trimestre
            Collections.sort(grades);

            for (int j = 0; j < grades.size(); j++) {
                Grade grade = grades.get(j);
                cell = row.createCell(j + 4);
                cell.setCellValue(grade.getGrade());
            }
        }
    }

    private void escreverAba2(Workbook wb,
                              List<Student> students) {
        CreationHelper ch = wb.getCreationHelper();
        Sheet sheet = wb.createSheet(WorkbookUtil.createSafeSheetName("Planilha2"));

        CellStyle decimalStyle = wb.createCellStyle();
        decimalStyle.setDataFormat(wb.createDataFormat().getFormat("0.00"));

        Font cellFont = wb.createFont();
        cellFont.setBold(true);

        CellStyle cellBoldStyle = wb.createCellStyle();
        cellBoldStyle.setFont(cellFont);

        // Criando cabecalho
        // Create a row and put some cells in it. Rows are 0 based.
        Row row = null;
        row = sheet.createRow(0);

        // Create a cell and put a value in it.
        Cell cell = null;

        cell = row.createCell(0);
        cell.setCellValue(ch.createRichTextString("Identificação"));
        cell.setCellStyle(cellBoldStyle);

        cell = row.createCell(1);
        cell.setCellValue(ch.createRichTextString("Nome"));
        cell.setCellStyle(cellBoldStyle);

        cell = row.createCell(2);
        cell.setCellValue(ch.createRichTextString("Idade"));
        cell.setCellStyle(cellBoldStyle);

        cell = row.createCell(3);
        cell.setCellValue(ch.createRichTextString("Média das Notas"));
        cell.setCellStyle(cellBoldStyle);

        // Ordenando por idade
        Collections.sort(students, Student.ageComparator());

        // Criando linhas
        for (int i = 0; i < students.size(); i++) {
            Student student = students.get(i);

            row = sheet.createRow(i + 1);

            cell = row.createCell(0);
            cell.setCellValue(student.getCode());

            cell = row.createCell(1);
            cell.setCellValue(ch.createRichTextString(student.getName()));

            cell = row.createCell(2);
            cell.setCellValue(student.getIdade());

            cell = row.createCell(3);
            cell.setCellValue(student.getMediaNotas());
            cell.setCellStyle(decimalStyle);
        }
    }

    private void escreverAba3(Workbook wb,
                              List<Student> students) {
        // Criterio de aprovacao = 70%
        double criterioAprovacao = 70;
        double idade30Anos = 30;
        double qtdAlunos = students.size();
        double qtdAlunosMasculinos = students.stream().filter(s -> Student.SEX_M.equals(s.getSex())).count();
        double qtdAlunosFemininos = students.stream().filter(s -> Student.SEX_F.equals(s.getSex())).count();
        double qtdAlunosMenores30Anos = students.stream().filter(s -> s.getIdade().intValue() <= idade30Anos).count();
        double qtdAlunosAprovados = students.stream().filter(s -> s.getSomaNotas() >= criterioAprovacao).count();

        OptionalDouble mediaNotaAlunosMaiores30Anos = students.stream().filter(s -> s.getIdade().intValue() > idade30Anos)
                .mapToDouble(s -> s.getGrades().stream().mapToDouble(g -> g.getGrade()).sum()).average();

        OptionalDouble mediaNotaAlunosMasculinos = students.stream().filter(s -> Student.SEX_M.equals(s.getSex()))
                .mapToDouble(s -> s.getGrades().stream().mapToDouble(g -> g.getGrade()).sum()).average();

        OptionalDouble mediaNotaAlunosFemininos = students.stream().filter(s -> Student.SEX_F.equals(s.getSex()))
                .mapToDouble(s -> s.getGrades().stream().mapToDouble(g -> g.getGrade()).sum()).average();

        OptionalDouble mediaIdade = students.stream().mapToInt(s -> s.getIdade()).average();

        CreationHelper ch = wb.getCreationHelper();
        Sheet sheet = wb.createSheet(WorkbookUtil.createSafeSheetName("Planilha3"));

        CellStyle decimalStyle = wb.createCellStyle();
        decimalStyle.setDataFormat(wb.createDataFormat().getFormat("0.00"));

        CellStyle pctStyle = wb.createCellStyle();
        pctStyle.setDataFormat(wb.createDataFormat().getFormat("0%"));

        Font cellFont = wb.createFont();
        cellFont.setBold(true);

        CellStyle cellBoldStyle = wb.createCellStyle();
        cellBoldStyle.setFont(cellFont);

        // Criando cabecalho
        // Create a row and put some cells in it. Rows are 0 based.
        Row row = null;
        row = sheet.createRow(0);

        // Create a cell and put a value in it.
        Cell cell = null;

        cell = row.createCell(0);
        cell.setCellValue(ch.createRichTextString("Estatísticas"));
        cell.setCellStyle(cellBoldStyle);

        row = sheet.createRow(1);

        cell = row.createCell(0);
        cell.setCellValue(ch.createRichTextString("Percentual de alunos do sexo masculino"));

        cell = row.createCell(1);
        cell.setCellValue(qtdAlunosMasculinos / qtdAlunos);
        cell.setCellStyle(pctStyle);

        row = sheet.createRow(2);

        cell = row.createCell(0);
        cell.setCellValue(ch.createRichTextString("Percentual de alunos do sexo feminino"));

        cell = row.createCell(1);
        cell.setCellValue(qtdAlunosFemininos / qtdAlunos);
        cell.setCellStyle(pctStyle);

        row = sheet.createRow(3);

        cell = row.createCell(0);
        cell.setCellValue(ch.createRichTextString("Percentual de alunos com menos de 30 anos"));

        cell = row.createCell(1);
        cell.setCellValue(qtdAlunosMenores30Anos / qtdAlunos);
        cell.setCellStyle(pctStyle);

        row = sheet.createRow(4);

        cell = row.createCell(0);
        cell.setCellValue(ch.createRichTextString("Percentual de alunos aprovados"));

        cell = row.createCell(1);
        cell.setCellValue(qtdAlunosAprovados / qtdAlunos);
        cell.setCellStyle(pctStyle);

        row = sheet.createRow(5);

        cell = row.createCell(0);
        cell.setCellValue(ch.createRichTextString("Média de nota dos alunos com mais de 30 anos"));

        cell = row.createCell(1);
        cell.setCellValue(mediaNotaAlunosMaiores30Anos.orElse(0));
        cell.setCellStyle(decimalStyle);

        row = sheet.createRow(6);

        cell = row.createCell(0);
        cell.setCellValue(ch.createRichTextString("Média de nota dos alunos do sexo masculino"));

        cell = row.createCell(1);
        cell.setCellValue(mediaNotaAlunosMasculinos.orElse(0));
        cell.setCellStyle(decimalStyle);

        row = sheet.createRow(7);

        cell = row.createCell(0);
        cell.setCellValue(ch.createRichTextString("Média de nota dos alunos do sexo feminino"));

        cell = row.createCell(1);
        cell.setCellValue(mediaNotaAlunosFemininos.orElse(0));
        cell.setCellStyle(decimalStyle);

        row = sheet.createRow(8);

        cell = row.createCell(0);
        cell.setCellValue(ch.createRichTextString("Média de idade dos participantes da base"));

        cell = row.createCell(1);
        cell.setCellValue(mediaIdade.orElse(0));
        cell.setCellStyle(decimalStyle);

    }

}
